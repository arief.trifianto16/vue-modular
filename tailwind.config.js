module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      avenir: ['Avenir'],
      apercu: ['Apercu']
    },
    colors: {
      transparent: 'transparent',
      black: '#000000',
      white: '#ffffff',
      'whitesmoke-lighter': '#fafafa',
      whitesmoke: '#f4f7f9',
      'whitesmoke-darker': '#e4ebf0',

      'primary-blue-lighter': '#7190fd',
      'primary-blue': '#1346fd',
      'primary-blue-darker': '#113fe3',

      'primary-mango': '#f49c31',

      'secondary-slime': '#91cb0e',

      'secondary-maize': '#ffc550',

      'secondary-peachy': '#f98d8d',

      'gray-title': '#4a4a4a',
      'gray-text': '#666666',
      'greyish-brown': '#444444',
      greyish: '#a6a6a6',
      'pinkish-grey': '#bbbbbb',
      'gray-warm': '#888888',
      'gray-light': 'rgba(155, 155, 155, 0.5)',
      'gray-dark': '#aaaaaa',
      'gray-disable': '#d0d0d0',
      'gray-border': '#dedede',

      red: '#fe2a5c'
    },
    boxShadow: {
      default: '0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06)',
      md: '0 4px 6px -1px rgba(0, 0, 0, .1), 0 2px 4px -1px rgba(0, 0, 0, .06)',
      lg: '0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05)',
      xl: '0 0 10px 2px rgba(187, 187, 187, 0.5)',
      none: 'none'
    },
    borderRadius: {
      none: '0',
      DEFAULT: '.25rem',
      lg: '.5rem',
      xl: '25px',
      full: '9999px',
      large: '15px'
    },
    zIndex: {
      '0': 0,
      '10': 10,
      '40': 40,
      '50': 50,
      '60': 60,
      '70': 70,
      '80': 80,
      auto: 'auto'
    },
    extend: {
      height: {
        '14': '3.25rem',
        '72': '17rem',
        '80': '18rem',
        '104': '26rem',
        'screen-75': '75vh',
        'screen-55': '55vh',
        'screen-45': '45vh',
        'screen-25': '25vh',
        'screen-43': '43%',
        'screen-57': '57%',
        'screen-85': '85vh'
      },
      maxHeight: {
        'screen-80': '80vh',
        'screen-83': '83vh',
        'screen-72': '72vh',
        '56': '14rem'
      },
      fontSize: {
        xxs: '.625rem'
      },
      width: {
        '80': '20rem'
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}