window.variables = {
    // apiUrl: '', // untuk wgs
    apiUrl: 'https://voaqa.gsit-erp.com/api/', //untuk bca
    // apiUrl: 'https://voadev.gsit-erp.com/api/', //untuk bca
    // apiUrl: 'http://10.21.248.89:8080/api/',
    // apiUrl: 'http://10.21.248.204:8080/',
    streamUrl: 'https://voadev.gsit-erp.com/track',
    googleApiKey: 'AIzaSyBWYXbqv3JeiEWu4zTKBUTaMLhR5b_T7_k',
    vapidKey: 'BBYCxwATP2vVgw7mMPHJfT6bZrJP2iUV7OP_oxHzEcNFenrX66D8G34CdEmVULNg4WJXfjkeyT0AT9LwavpN8M4=',
    // temporaryToken: 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJaZGlTam93V182RF9iSmpVM0xIamZsSEQ4WHRsNDY1U0Q5VnJ2djFZdkFBIn0.eyJqdGkiOiJlYWE4Nzg1OS1jMjhkLTRlMDAtYTA1Yi1kYTg2NmMzOGViYjAiLCJleHAiOjE1NjM4MDIxMDYsIm5iZiI6MCwiaWF0IjoxNTYzNzY2MTA2LCJpc3MiOiJodHRwOi8va2V5Y2xvYWstZXJwYXV0aC5kZXZvcGVuc2hpZnQuZ3NpdC5jby5pZC9hdXRoL3JlYWxtcy9tb2JpbGVfbG9naXN0aWMiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiMzhhYTM1MzctZTcwZi00MGFhLWI5N2EtZDgxZjkzZTNiODM0IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoidm9hLWFwaSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6ImE0YzZhMmVmLTYyYmItNDAyMi1hYTU3LTNhN2VmNzc1N2UxZSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cDovL2xvY2FsaG9zdDo4MDgxIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJpbnRlcm5hbCIsInVzZXIiLCJyb2xlXzEiXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6InByb2ZpbGUgZW1haWwiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJCaWxseSBTcGFya3MiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJiaWxseSIsImdpdmVuX25hbWUiOiJCaWxseSIsImZhbWlseV9uYW1lIjoiU3BhcmtzIiwiZW1haWwiOiJiaWxseS5zcGFya3NAZ21haWwuY29tIn0.P0qQyyNNPsfCOk-RoaTfNRLHFYx71SfLCJxyAtwENtpasBk6aZR_F8rhERvlqKtPjyhv5IAU3VcHl5247XRKBa5JPhSnjumGu300IQyOL8BJr6D7SjjXXEuXqc2oO-UT_ljMvKQEn76dCvpB6S8OR099EHruYz0KPE09xAVc0m185OkIZE2JzqkEyfn-2IaFA78-osbv_Iz2SGdoct9XilHlDxRzM4cGwBsiQ7Jfef-FTC-mAKZI8IEtADUisAjQOPj8XV7Ocax4YT--lyqeCU408E4SiDIaHATq0W5yG9OtroVxgFA7x-1Fx-GvJR7RCufYHhhEKFOg6fqQTPiWaQ',
    useKeycloak: true,
    intervalUpdateLocation: 60000,
    authServer: {
        url: 'https://voaqa.gsit-erp.com/auth',
        // url: 'https://voadev.gsit-erp.com/auth',
        realm: 'mobile_logistic_qa',
        // realm: 'mobile_logistic',
        clientId: 'voa-api',
        onLoad: 'login-required'
    }
}