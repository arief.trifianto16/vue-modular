export default class Formater {

    constructor() {
        this.name = 'Formater';
    }

    formatDate(tanggal, jam) {
        let myDays = [
            'Minggu',
            'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu'
        ]

        let date =
            tanggal.toString().split('/')[1] +
            '/' +
            tanggal.toString().split('/')[0] +
            '/' +
            tanggal.toString().split('/')[2]

        var curDate = new Date(date + ' ' + jam)
        var thisDay = curDate.getDay()
        thisDay = myDays[thisDay]

        date = new Date(date + ' ' + jam).toString()

        return (
            thisDay +
            ' ' +
            date.split(' ')[2] +
            ' ' +
            date.split(' ')[1] +
            ', ' +
            jam
        )
    }

    formatJenisOrder(kodeJenis, descJenis, nomorRutin) {
        let ketRutin = "";
        let infoJenisOrder = "";
        if (nomorRutin !== null && nomorRutin !== "-") {
            ketRutin = " | ROUTINE";
        }
        if (kodeJenis == "FX" || kodeJenis == "HR") {
            infoJenisOrder = "REGULAR TRIP • " + descJenis + ketRutin;
        } else if (kodeJenis == "DL") {
            infoJenisOrder = "PACKAGE TRIP • " + descJenis + ketRutin;
        } else if (kodeJenis == "AD") {
            infoJenisOrder = "ADDITIONAL DRIVER";
        }
        return infoJenisOrder.toUpperCase();
    }

    getInitials(name) {
        let names = name.split(" "),
            initials = names[0].substring(0, 1).toUpperCase();

        if (names.length > 1) {
            initials += names[names.length - 1].substring(0, 1).toUpperCase();
        }
        return initials;
    }
}