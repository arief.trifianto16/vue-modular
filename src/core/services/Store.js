export default class Store {

    constructor() {
        this.name = 'Store';
        this.modules = {}
    }

    registerModuleStore(module = {}) {
        this.modules[module.name] = module
    }

    getModules() {
        return this.modules;
    }
}