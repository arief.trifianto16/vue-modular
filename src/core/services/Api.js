/* eslint no-undef: 0 */
/* eslint no-unused-vars: 0 */

import axios from 'axios';
import {
    stringify
} from 'query-string';
import EventEmitter from 'events';


export default class Api {

    constructor() {
        this.name = 'Api';

        this.config = {
            baseURL: window.variables.apiUrl
        }

        this.axios = axios.create(this.config)

        this.configureAxios();

        this.event = new EventEmitter();
    }

    async get(urlPath, queryParams) {
        const hasQueryParams = queryParams !== undefined;
        const path = !hasQueryParams ?
            urlPath :
            urlPath + '?' + stringify(queryParams);
        return await this.axios.get(path);
    }

    async post(id, params, body = {}) {
        const path = this._buildUrlById(id, params, 'post');
        return await this.axios.post(path, body);
    }

    async put(id, params, body = {}) {
        const path = this._buildUrlById(id, params, 'put');
        return await this.axios.put(path, body);
    }

    async patch(id, params, body = {}) {
        const path = this._buildUrlById(id, params, 'patch');
        return await this.axios.patch(path, body);
    }

    async delete(id, params, body = {}, queryParams) {
        const hasQueryParams = queryParams !== undefined;
        const urlPath = this._buildUrlById(id, params, 'delete');
        const path = !hasQueryParams ?
            urlPath :
            urlPath + '?' + stringify(queryParams);

        return await this.axios.delete(path, body);
    }

    onError(cb) {
        this.event.on('error', (msg) => {
            return cb(msg);
        });
    }

    configureAxios() {
        this.axios.interceptors.request.use(
            (config) => {
                let token = ''
                token = window.keycloak.token
                if (token) {
                    config.headers = {
                        Authorization: `Bearer ${token}`
                    }
                }
                // Do something before request is sent
                return config
            },
            (error) => {
                // Do something with request error
                return Promise.reject(error)
            }
        )

        this.axios.interceptors.response.use(
            (response) => {
                return response;
            },
            (error) => {
                // if (errorStatus === 401) {
                //     this.event.emit('logout');
                //     this.user = null;
                //     return;
                // }
                const errorStatus = error.response.status;
                if (errorStatus === 500 || errorStatus === 400 || errorStatus === 404 || errorStatus === 403) {
                    this.event.emit('error', error.response.data);
                    return;
                }
                return
            }
        );
    }

    _buildUrlById(operationId, params, methodType) {
        const {
            basePath,
            paths
        } = this.schema;

        const methodPath = Object.keys(paths).find(path => {
            const methods = paths[path][methodType];
            return methods !== undefined && methods['operationId'] === operationId;
        });

        if (!methodPath) {
            throw new Error(`Operation ID not found: '${operationId}'`);
        }

        return this._buildPathWithParams(`${basePath}${methodPath}`, params);
    }

    _buildPathWithParams(path, params) {
        if (!params) {
            return path;
        }

        const matches = path.match(/{\w+}/g);

        if (!matches) {
            return path;
        }

        matches.forEach(match => {
            const propName = match.replace(/({|})/g, '');
            path = path.replace(match, params[propName]);
        });

        return path;
    }
}