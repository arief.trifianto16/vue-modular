export default class Component {
    constructor() {
        this.name = 'Component';

        this.components = {
            layout: [],
            base: [],
        };
    }

    register(Component, type = 'common') {
        const name = Component.name
        if (name === undefined) {
            throw 'Missing prop name in component';
        }

        this.components[type].push({
            name: name,
            component: Component
        });

        return this.components;
    }

    getComponent(type, name) {
        return this.components[type].find(com => com.name === name);
    }

    getComponents() {
        return this.components
    }

}