class Service {
    constructor() {
        this._serviceNames = [];
    }

    registerService(Service) {
        const service = new Service(this);
        const serviceName = service.name;

        if (this._serviceNames.includes(serviceName)) {
            throw `service ${serviceName} exist`;
        }

        this._serviceNames.push(serviceName);
        this[serviceName] = service;
    }
}

const service = new Service();

const services = []
const requireServices = require.context('./services', true, /\.js$/)

requireServices.keys().forEach(fileName => {
    const file = requireServices(fileName)
    services.push(file.default)
})

for (let i in services) {
    service.registerService(services[i]);
}

export default service;