// const modules = [
//     require('@/modules/order/index.main.js')
// ];

export async function run(service) {
    const modules = []
    const requireModules = require.context('@/modules', true, /\.main\.js$/)
    requireModules.keys().forEach(fileName => {
        const module = requireModules(fileName)
        modules.push(module)
    })

    const startModules = modules.map(module => {
        return module.default(service);
    });

    return await Promise.all(startModules);
}