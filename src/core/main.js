import {
    createApp
} from "vue";
import {
    createRouter,
    createWebHistory
} from 'vue-router'
import {
    createStore
} from 'vuex'
import App from "@/core/App.vue";

import "./assets/style/tailwind.css";
import './assets/style/fonts.css'
import './assets/style/iconfonts.css'

import "./plugins/PWA/registerServiceWorker";
import keycloak from "./plugins/keycloak";

import {
    run
} from './bootstrap';
import service from './service';
import {
    registerGlobalComponents
} from './componentRegister';
// import mitt from 'mitt';

var app = createApp(App);

app.config.globalProperties.$variables = window.variables;

registerGlobalComponents(service);
const components = service.Component.getComponents();
components.base.forEach(base => {
    app.component(base.name, base.component)
})
components.layout.forEach(layout => {
    app.component(layout.name, layout.component)
})

app.use(keycloak)

app.config.globalProperties.$service = service;

console.log(service.Api);

// const emitter = mitt();
// app.config.globalProperties.$emitter = emitter

app.keycloak
    .init({
        onLoad: "login-required",
    })
    .success((auth) => {
        if (!auth) {
            window.location.reload();
        } else {
            run(service).then(() => {
                const routes = service.Router.getRoutes();
                const router = createRouter({
                    history: createWebHistory(),
                    routes
                })
                app.use(router)

                const modules = service.Store.getModules()
                const store = createStore({
                    modules: modules
                })
                app.use(store)
                store.$api = service.Api
                store.$keycloak = app.keycloak

                app.mount("#app");
            })

        }

        //Token Refresh
        setInterval(() => {
            app.keycloak
                .updateToken(70)
                .success((refreshed) => {
                    if (refreshed) {
                        console.log("refreshed token");
                    } else {
                        console.log(
                            "Token not refreshed, valid for " +
                            Math.round(
                                app.keycloak.tokenParsed.exp +
                                app.keycloak.timeSkew -
                                new Date().getTime() / 1000
                            ) +
                            " seconds"
                        );
                    }
                })
                .error(() => {
                    console.log("failed to refresh token");
                });
        }, 60000);
    })
    .error(() => {
        console.log("authentication failed");
    });