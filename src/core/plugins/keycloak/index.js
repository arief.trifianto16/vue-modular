import * as Keycloak from "./keycloak";

var authOptions = {
    url: window.variables.authServer.url,
    realm: window.variables.authServer.realm,
    clientId: window.variables.authServer.clientId
};

var _keycloak = Keycloak(authOptions);

export default {
    install: (app) => {
        app.keycloak = _keycloak
        window.keycloak = _keycloak

        Object.defineProperties(app.config.globalProperties, {
            keycloak: {
                get() {
                    return _keycloak
                }
            },
            $keycloak: {
                get() {
                    return _keycloak
                }
            }
        })
    }
}