const ProfileModule = () => import( /* webpackChunkName: "ProfileModule" */ '../ProfileModule.vue');
const Account = () => import( /* webpackChunkName: "ProfileAccount" */ '../pages/account/Account.vue');
const ChangePassword = () => import( /* webpackChunkName: "ProfilePassword" */ '../pages/changePassword/ChangePassword.vue');

export const Routes = [{
    path: "/profile",
    name: "profile-module",
    component: ProfileModule,
    children: [{
            path: "account",
            name: "profile-account",
            component: Account,
        },
        {
            path: "change-password",
            name: "profile-password",
            component: ChangePassword,
        }
    ]
}]