/* eslint no-undef: 0 */
/* eslint no-unused-vars: 0 */

export const GET_FULL_NAME = "profile/GET_FULL_NAME"
export const GET_INITIAL = "profile/GET_INITIAL"
export const GET_JOB_PLACE = "profile/GET_JOB_PLACE"

export const FETCH_PROFILE = "profile/FETCH_PROFILE"
export const FETCH_LOGOUT = "profile/FETCH_LOGOUT"

export const Store = {
    name: "profile",
    state: {
        profile: {}
    },
    mutations: {
        setProfile: (state, payload) => {
            state.profile = payload
        }
    },
    getters: {
        [GET_FULL_NAME]: state => {
            let fullName = ''

            if (Object.keys(state.profile).length > 0)
                fullName = state.profile.namaDepan + ' ' + state.profile.namaBelakang

            return fullName
        },

        [GET_INITIAL]: state => {
            let initialName = ''

            if (Object.keys(state.profile).length > 0) {
                initialName += state.profile.namaDepan.charAt(0).toUpperCase()
                initialName += state.profile.namaBelakang.charAt(0).toUpperCase()
            }

            return initialName
        },

        [GET_JOB_PLACE]: state => {
            let jobAt = state.profile.namaCabang
            // if (state.type === 'external') jobAt = state.profile.namaVendor

            return jobAt
        },
    },
    actions: {
        async [FETCH_PROFILE]({
            commit
        }) {
            const response = await this.$api.get('/profile/internal')
            commit('setProfile', response.data.data)
        },

        [FETCH_LOGOUT]() {
            this.$keycloak.logout()
        }
    }
}