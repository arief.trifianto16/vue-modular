import {
    Store
} from './store'
import {
    Routes
} from './router'

export default async function (service) {
    const storeService = service.Store

    storeService.registerModuleStore(Store)

    const routerService = service.Router;

    routerService.registerModuleRoutes(Routes);

}