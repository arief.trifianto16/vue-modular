/* eslint no-undef: 0 */
/* eslint no-unused-vars: 0 */

export const FETCH_LIST_ORDER = "order/FETCH_LIST_ORDER"
export const FETCH_DETAIL_ORDER = "order/FETCH_DETAIL_ORDER"

export const Store = {
    name: "order",
    state: {

    },
    mutations: {

    },
    getters: {

    },
    actions: {
        async [FETCH_LIST_ORDER]({
            commit
        }, payload) {
            const response = await this.$api.get('/transaction/order/pic', payload)
            return response.data.data
        },

        async [FETCH_DETAIL_ORDER]({
            commit
        }, payload) {
            const nomorOrder = encodeURIComponent(payload).replace(/'/g, '%27')
            const response = await this.$api.get(`/transaction/order/pic/${nomorOrder}`)
            return response.data.data
        }
    }
}