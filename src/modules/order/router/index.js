const OrderModule = () => import( /* webpackChunkName: "OrderModule" */ '../OrderModule.vue');
const OrderList = () => import( /* webpackChunkName: "OrderList" */ '../pages/list/OrderList.vue');
const OrderDetail = () => import( /* webpackChunkName: "OrderDetail" */ '../pages/detail/OrderDetail.vue');
const OrderCreate = () => import( /* webpackChunkName: "OrderCreate" */ '../pages/create/OrderCreate.vue');


export const Routes = [{
    path: "/transaction/order",
    name: "order-module",
    component: OrderModule,
    children: [{
            path: ":tabChoosen",
            name: "list-order-tab",
            components: {
                page: OrderList
            },
            meta: {
                showModal: false
            },
        },
        {
            path: ":tabChoosen/:nomorOrder",
            name: "detail-order",
            components: {
                page: OrderList,
                modal: OrderDetail
            },
            meta: {
                showModal: true
            }
        },
        {
            path: "create",
            name: "create-order",
            components: {
                page: OrderList,
                modal: OrderCreate
            },
            meta: {
                showModal: true
            },
        }
    ]
}]