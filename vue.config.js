// module.exports = {
//     pages: {
//         index: {
//             // entry for the page
//             entry: 'src/core/main.js'
//         }
//     }
// }

const path = require("path");

module.exports = {
    chainWebpack: config => {
        config
            .entry("app")
            .clear()
            .add("./src/core/main.js")
            .end();
        config.resolve.alias
            .set("@", path.join(__dirname, "./src"))

        config.plugins.delete('prefetch');
    }
};